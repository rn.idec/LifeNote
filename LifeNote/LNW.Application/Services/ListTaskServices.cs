﻿using LNW.Application;
using LNW.Business;
using System;
using System.Collections.Generic;
using System.Text;

namespace LNW.Application
{
    public class ListTaskServices : IListTaskServices
    {
        IListTaskRepository _repoListTask;
        public ListTaskServices(IListTaskRepository repoListTask)
        {
            _repoListTask = repoListTask;
        }
        public List<ListTask> GetAllListTask()
        {
            List<ListTask> listLTask = _repoListTask.GetAll();
            return listLTask;
        }
    }
}
