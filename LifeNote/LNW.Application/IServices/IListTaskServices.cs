﻿using LNW.Business;
using System;
using System.Collections.Generic;
using System.Text;

namespace LNW.Application
{
    public interface IListTaskServices
    {
        List<ListTask> GetAllListTask();
    }
}
