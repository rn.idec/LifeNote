﻿using LNW.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace LNW.Business
{
    public class MonthTask : ValueObject<MonthTask>
    {
        public int NumbersOfDay { get; set; }
        public string StartFrom { get; set; }
        public int YearNumber { get; set; }
        //list of DayTask
        public List<DayTask> ListDayTask { get; set; }
        //list of ListTask
        public List<ListTask> ListOfListTask { get; set; }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            throw new NotImplementedException();
        }
    }
}
