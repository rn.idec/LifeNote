﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LNW.Business
{
    public class NoteFactory
    {
        public static Note Create(TimeSpan _start, Nullable<System.TimeSpan> _end, string _description, 
            Guid _listTaskId)
        {
            return Create(Guid.NewGuid(), _start, _end, _description, _listTaskId);
        }

        public static Note Create(Guid guid, TimeSpan start,  Nullable<System.TimeSpan> end, string description,
            Guid listTaskId)
        {
            if (string.IsNullOrEmpty(description))
                throw new ArgumentNullException("description");


            Note _note = new Note(guid)
            {
                TimeStart = start,
                TimeEnd = end,
                Description = description,
                ListTaskId = listTaskId
            };

            return _note;
        }
    }
}
