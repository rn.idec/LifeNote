﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LNW.Business
{
    public class WorkFactory
    {
        public static Work Create(TimeSpan _start, TimeSpan _end, Nullable<System.TimeSpan> _pauseStart,
            Nullable<System.TimeSpan> _pauseEnd, string _description, Guid _listTaskId)
        {
            return Create(Guid.NewGuid(), _start, _end, _pauseStart, _pauseEnd, _description, _listTaskId);
        }

        public static Work Create(Guid guid, TimeSpan start, TimeSpan end, Nullable<System.TimeSpan>
            pauseStart, Nullable<System.TimeSpan> pauseEnd, string description, Guid listTaskId)
        {
            if (string.IsNullOrEmpty(description))
                throw new ArgumentNullException("description");


            Work _work = new Work(guid)
            {
                TimeStart = start,
                TimeEnd = end,
                PauseStart = pauseStart,
                PauseEnd = pauseEnd,
                Description =description,
                ListTaskId = listTaskId
            };

            return _work;
        }
    }
}
