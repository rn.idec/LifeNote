﻿using LNW.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;

namespace LNW.Business
{
    public class Work : Entity<Guid>
    {
        public Work()
        {

        }
        public Work(Guid id) : base()
        {
            Id = id;
        }
        public TimeSpan TimeStart { get; set; }
        public TimeSpan TimeEnd { get; set; }
        public Nullable<TimeSpan> PauseStart { get; set; }
        public Nullable<TimeSpan> PauseEnd { get; set; }
        public string Description { get; set; }
        public Guid ListTaskId { get; set; }

        public ListTask ListTask { get; set; }
    }
}
