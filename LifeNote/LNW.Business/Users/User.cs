﻿using LNW.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;

namespace LNW.Business
{
    public class User : Entity<Guid>
    {
        public User()
        {

        }
        public User(Guid _id) : base()
        {
            Id = _id;
        }
        public string Username { get; set; }
        public string MotDePasse { get; set; }
        public string Email { get; set; }
        public ICollection<ListTask> ListTasks { get; set; }
    }
}
