﻿using LNW.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;

namespace LNW.Business
{
    public class ListTask : Entity<Guid>
    {
        public ListTask()
        { }
        public ListTask(Guid _id) : base()
        {
            Id = _id;
        }
        public DateTime DateOfList { get; set; }
        public Guid UserId { get; set; }

        public User User { get; set; }
        public ICollection<Work> Works { get; set; }
        public ICollection<Note> Notes { get; set; }
        public ICollection<Objective> Objectives { get; set; }
    }
}
