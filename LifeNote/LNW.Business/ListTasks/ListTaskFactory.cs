﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LNW.Business
{
    public class ListTaskFactory
    {
        public static ListTask Create(DateTime _date, Guid _userId)
        {
            return Create(Guid.NewGuid(), _date, _userId);
        }

        public static ListTask Create(Guid guid, DateTime date, Guid userId)
        {
            ListTask Ltask = new ListTask(guid)
            {
                DateOfList = date,
                UserId = userId
            };


            return Ltask;
        }
    }
}
