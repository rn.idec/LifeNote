﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LNW.Infrastructure.Repository
{
    public interface IRepository<TEntity>
        where TEntity : Entity<Guid>
    {
        TEntity FindById(Guid id);
        List<TEntity> GetAll();
        void Add(TEntity entity);
        void Remove(TEntity entity);
    }
}
