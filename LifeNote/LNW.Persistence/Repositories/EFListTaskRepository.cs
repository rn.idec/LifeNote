﻿using LNW.Business;
using LNW.Persistence.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace LNW.Persistence.Repositories
{
    public class EFListTaskRepository : IListTaskRepository
    {
        private TaskContext _context;
        public EFListTaskRepository(TaskContext context)
        {
            _context = context;
        }

        public void Add(ListTask entity)
        {
            throw new NotImplementedException();
        }

        public ListTask FindById(Guid id)
        {
            throw new NotImplementedException();
        }

        public List<ListTask> GetAll()
        {
            List<ListTask> listLt = new List<ListTask>();
            foreach (var item in _context.ListTasks
                .Include(lt => lt.Works)
                .Include(lt =>lt.Notes)
                .Include(lt => lt.Objectives))
            {
                listLt.Add(item);
            }

            return listLt;
        }

        public void Remove(ListTask entity)
        {
            throw new NotImplementedException();
        }
    }
}
