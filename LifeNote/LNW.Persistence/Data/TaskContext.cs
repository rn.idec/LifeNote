﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using LNW.Business;

namespace LNW.Persistence.Data
{
    public class TaskContext : DbContext
    {
        public TaskContext(DbContextOptions<TaskContext> options) : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<ListTask> ListTasks { get; set; }
        public DbSet<Work> Works { get; set; }
        public DbSet<Note> Notes { get; set; }
        public DbSet<Objective> Objectives { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().ToTable("User");
            modelBuilder.Entity<ListTask>().ToTable("ListTask");
            modelBuilder.Entity<Work>().ToTable("Work");
            modelBuilder.Entity<Note>().ToTable("Note");
            modelBuilder.Entity<Objective>().ToTable("Objective");
        }
    }
}
